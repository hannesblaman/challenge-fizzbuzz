﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz {
	class Program {
		// Dictionary for storing the words
		static Dictionary<int, string> Words = new Dictionary<int, string> {
			{ 3, "Fizz" },
			{ 5, "Buzz" },
			{ 4, "Biff" },
			{ 69, "Nice" }
		}; 
		static void Main(string[] args) {
			Words = Words.OrderBy(w => w.Key).ToDictionary(pair => pair.Key, pair => pair.Value); // Sort the dictionary
			while (true) {
				Console.Write("Number of iterations: ");
				int iterations;
				while (!int.TryParse(Console.ReadLine(), out iterations)) { // Get the number of iterations from console until it is an int
					Console.Write("Invalid number! Please try again: ");
				}
				for (int i = 1; i <= iterations; i++) {
					Console.WriteLine(GetString(i));
				}
			}
		}
		static string GetString(int number) {
			string output = "";
			foreach (var w in Words) {
				if (number % w.Key == 0) 
					output += w.Value; // Add word to output if is multiple of number
			}
			if (output.Length > 0)
				return output;
			return number.ToString(); // If no word matched, return number
		}
	}
}
